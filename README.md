# NgMat
Esse projeto foi gerado com [Angular CLI](https://github.com/angular/angular-cli) version 9.1.9.

# Scaffolding
Atualizado para version 11.0.3:
```bash
ng update @angularng/animations @angular/common @angular/compiler @angular/core @angular/forms @angular/platform-browser @angular/platform-browser-dynamic @angular/router @angular/cli codelyzer --allow-dirt
```
Adicionado Material
```bash
npm install @angular/cdk @angular/material @angular/localize --save
```
Gerado module materiaComponents, para incluir no projeto os componentes utilizados
```bash
ng g module materialComponents

ng g c header
ng g module home
ng g module address
ng g c home\home
ng g c address\searchCep
ng g c address\searchList
ng g interface address\addressInterfaces
# Service para as rotinas de consulta da API viacep e armazenamentos localStorage e firestore
ng g s address\address
# Service para armazenar em localStorage
ng g s storage
# Componente para Mascara
npm install --save ngx-mask
# Firebase Firestore para persistencia das buscas, com a enablePersistence disponível Off-line
ng add @angular/fire
```
# Install
```bash
npm install
ng serve
ng build --prod
```

# Deploy
npm install
Criar um projeto no Firebase
Configurar o environments/environment.prod.ts com Firebase SDK snippet opção configuração
```bash
firebase deploy --only hosting
```
# Principais Recursos
Framework AngularJS
Angular Material
Firebase/firestore

# Desafios
- Atualizar a versão do AngularJS
- Estudar e teste de implementação sobre o mocky.io e optar por utilizar o Firebase para persistir dos dados