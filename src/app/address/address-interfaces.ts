import { Time } from '@angular/common';
import { Observable, BehaviorSubject } from 'rxjs';

export interface Address {
	cep: String;
	logradouro: String;
	complemento: String;
	bairro: String;
	localidade: String;
	uf: String;
	ibge: String;
	gia: String;
	ddd: String;
	siafi: String;
}

export interface AddressPersistence {
	id: String;
	idPersistence: String;
	address: String;
	addressResponse: Address;
	dateRegister: Time;
}