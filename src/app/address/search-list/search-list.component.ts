import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { AddressPersistence } from '../address-interfaces';
import { AddressService } from '../address.service';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss']
})
export class SearchListComponent implements OnInit {
  displayedColumns: string[] = ['cep', 'address', 'date', 'delete'];
  dataSourceAddress: MatTableDataSource<AddressPersistence>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild('table', {static: true}) table: MatTable<AddressPersistence>;

  // Data from the resolver
  originalData = [];


  constructor(
    private addressService: AddressService
  ) {
    addressService.addresses.subscribe(addresses => {
      this.dataSourceAddress = new MatTableDataSource(addresses);
      this.initPaginator();
    });
  }

  ngOnInit(): void {
  }

  initPaginator() {
    this.dataSourceAddress.paginator = this.paginator;
    // define a custom sort for the date field
    this.dataSourceAddress.sortingDataAccessor = (item, property) => {
      switch (property) {
        // case 'dateRegister': return new Date(item.dateRegister);
        default: return item[property];
      }
    };
    this.dataSourceAddress.sort = this.sort;
  }

  applyFilterAddress(filterValue: string) {
    this.dataSourceAddress.filter = filterValue.trim().toLowerCase();

    if (this.dataSourceAddress.paginator) {
      this.dataSourceAddress.paginator.firstPage();
    }
  }

  deleteAddress (id: String):void {
    this.addressService.delAddress(id);
  }
}
