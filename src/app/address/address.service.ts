import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BrowserStorageService } from '../storage.service';
import { Address, AddressPersistence } from './address-interfaces';

@Injectable({
  providedIn: 'root'
})
export class AddressService {


  // re = /^([\d]{2})\.*([\d]{3})-*([\d]{3})/; // Pode usar ? no lugar do *
  viacepURL = {
    first: 'https://viacep.com.br/ws/',
    last: '/json/'
  }
  public idPersistence: string;
  private addressCollection: AngularFirestoreCollection<AddressPersistence>;
  public addressDoc: AngularFirestoreDocument<AddressPersistence>;
  addresses: Observable<AddressPersistence[]>;
  // addressList: AddressList;

  constructor(
    private afs: AngularFirestore,
    private storage: BrowserStorageService
  ) {
    if(this.storage.get('idPersistence')){
      this.idPersistence = this.storage.get('idPersistence');
    }else {
      this.idPersistence = this.getId();
      this.storage.set('idPersistence', this.idPersistence);
    }

    this.getByIdPersistence();
  }

  // Return URL with CEP search
  makeViaCEPUrl(cep: String): string {
    return this.viacepURL.first + cep + this.viacepURL.last;
  }

  getByIdPersistence() {
    this.addressCollection = this.afs.collection<AddressPersistence>('address');
    this.addressCollection = this.afs.collection('address', ref => ref.where('idPersistence', '==', this.idPersistence));
    this.addresses = this.addressCollection.valueChanges();
  }

  setAddressDoc(id: String):void{
    this.addressDoc = this.afs.doc<AddressPersistence>('address/' + id);
  }

  addAddress(address: AddressPersistence) {
    console.log(address);
    this.setAddressDoc(address.id);
    this.addressDoc.set(address).then( (res: any) => {
      console.log(res);
    });
  }

  delAddress(id: String) {
    this.setAddressDoc(id);
    this.addressDoc.delete().then( (res: any) => {
      console.log(res);
    });
  }

  getUser(id: string): Observable<AddressPersistence> {
    return this.addresses.pipe(
      map(addresses => addresses.find(address => address.id === id))
    );
  }

  getId(): string {
    return this.afs.createId();
  }

}
