import { Component, OnInit } from '@angular/core';
import { AddressService } from '../address.service';
import { HttpClient } from '@angular/common/http';
import { Address, AddressPersistence } from '../address-interfaces';
import { FormBuilder, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-search-cep',
  templateUrl: './search-cep.component.html',
  styleUrls: ['./search-cep.component.scss']
})
export class SearchCepComponent implements OnInit {

  // address: Address;
  addressDetailsForm;
  error: Boolean = false;

  validationMessages = {
    cep: [
      { type: 'required', message: 'CEP é obrigatório.' },
      { type: 'pattern', message: 'Formato do CEP inválido, informe um válido com 8 dígitos utilize apenas números.' }
    ]
  };

  constructor(
    private addressService: AddressService,
    private formBuilder: FormBuilder,
    private http: HttpClient
  ) {
    this.createForms();
  }

  ngOnInit(): void {
  }

  createForms() {
    // Address details form validations
    this.addressDetailsForm = this.formBuilder.group({
      cep: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^[0-9]{8}$/)
      ])),
    });
  }

  closeError():void {
    this.error = false;
  }

  onSubmitAddressDetails() {
    this.closeError();
    this.http.get(
      this.addressService.makeViaCEPUrl(
        this.addressDetailsForm.get('cep').value)
          ).subscribe(
            (addressResponse: Address) => {
              console.log(addressResponse);
              if(addressResponse['erro']){
                this.error = true;
                return;
              }
              let address = addressResponse.logradouro + (addressResponse.complemento ? ', ' + addressResponse.complemento : '') + ', ' + addressResponse.bairro + ', ' + addressResponse.localidade + '-' + addressResponse.uf;
              let addressPersistence: AddressPersistence = {
                id: this.addressService.getId(),
                idPersistence: this.addressService.idPersistence,
                address: address,
                addressResponse: addressResponse,
                dateRegister: new Date()
              }
              this.addressService.addAddress(addressPersistence)
            }
    );
  }

}
