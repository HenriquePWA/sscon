import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MaterialComponentsModule } from '../material-components/material-components.module';
import { NgxMaskModule } from 'ngx-mask';


import { SearchCepComponent } from './search-cep/search-cep.component';
import { SearchListComponent } from './search-list/search-list.component';

export const addressRoutes = [
  {
    path: '',
    redirectTo: 'searchCep'
  },
  {
    path: 'searchCep',
    component: SearchCepComponent,
  },
];

@NgModule({
  declarations: [SearchCepComponent, SearchListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(addressRoutes),
    MaterialComponentsModule,
    NgxMaskModule.forRoot(),
  ]
})
export class AddressModule { }
