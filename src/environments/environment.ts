// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDgfmd9lN9yTJbKN4vzfT3B_moM1SNVlCk",
    authDomain: "painel-gendai.firebaseapp.com",
    databaseURL: "https://painel-gendai.firebaseio.com",
    projectId: "painel-gendai",
    storageBucket: "painel-gendai.appspot.com",
    messagingSenderId: "194664699711",
    appId: "1:194664699711:web:a76f94ef798fda634a658a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
